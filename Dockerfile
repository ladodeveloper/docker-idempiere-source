FROM maven:3.6-jdk-11

LABEL maintainer="saul.pina@ingeint.com"

ENV IDEMPIERE_VERSION master
ENV IDEMPIERE_REPOSITORY /idempiere

RUN apt-get update && apt-get install -y --no-install-recommends make && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/idempiere/idempiere.git && cd $IDEMPIERE_REPOSITORY 
RUN cd $IDEMPIERE_REPOSITORY && mvn verify -U
